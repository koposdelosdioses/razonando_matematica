# Razonando matemática
Es un proyecto desarrollado dentro del marco estudiantil de Programa tu Futuro, consiste en mejorar la forma de educar a niños de primaria y secundaria, pudiendo compartir contenido educativo personalizado.

## Conocimientos previos

Se utilizarán FrameWorks como Bootstrap en css y Laravel para PHP

## Instalación

Nos aseguramos tener Xampp instalado, si no lo tengo instalado lo descargo desde acá:

[Xampp](https://www.apachefriends.org/es/index.html).

En el boton de inicio de windows nos tendria que aparecer ( en algun lugar ) el icono de `Xampp Control Panel`
Lo abrimos y colocamos Start al apache. Se tiene que poner verde.

Volvemos a la consola y navegamos las carpetas hasta pararnos en `C:\xampp\htdocs` para eso tenemos que colocar el comando `cd..` hasta estar en el disco C  y luego colocar `cd xampp` y luego `cd htdocs` `C:\xampp\htdocs` y colocar la url que obtuvimos del repositorio de [bitbucket](https://bitbucket.org/) con el prefijo `git clone ` quedando algo así:

`git clone https://XXXX@bitbucket.org/koposdelosdioses/razonando_matematica.git`

Se va a clonar el proyecto y cuando entre a `localhost/razonando_matematica` en el chrome se va a ver la web como la tenemos hasta ahora.

Para todo lo anterior se asume que ya tienen cuenta en [bitbucket](https://bitbucket.org/) y tienen el el [git](https://git-scm.com/) instalado y configurado con usuario y mail