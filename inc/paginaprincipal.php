<section id="pagina principal">
    <div class="container">
        <div class="row">
            <div id="page" class="container-fluid">
                <div class="pull-xs-right context-header-settings-menu">
                </div>
                <div id="contenido de la pagina" class="row">
                    <div id="region-main-box" class="col-xs-12">
                        <section id="region-main">
                            <div class="card card-block">
                                <div class="abaci-main">
                                    <div class="jumbotron jumbotron-fluid" style="background: url('/theme/codoacodo/pix/jumbotron.jpg') center center;background-size:cover;height: 352px;">
                                        <div class="jumbotron-overlay">
                                            <div class="container" style="color:white; background-color: rgba(44,44,44,0.35);">
                                                <h1 class="text-center">Aula Virtual abaci club</h1>
                                                <h4 class="text-center">Plataforma complementaria.</h4>
                                                <h4 class="text-center">Contenidos Matemáticos.</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="jumbotron jumbotron-fluid">
                                        <div class="container">
                                            <p class="lead">Pasen y vean! </p>
                                            <p class="lead"> Aprendan y divulguen - Entender es mas importante que saber!!!</p>
                                        </div>
                                    </div>
                                    <div class="container m-t-3">
                                        <div class="row">
                                            <a href="/course/view.php?id=11" class="col-md-3 col-sm-6 col-xs-12 text-center ">
                                                <span class="fa-stack fa-5x">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-superscript fa-stack-1x fa-inverse"></i>
                                                </span>
                                                <h3 class="text-center">Mate 1 prov.</h3>
                                            </a>
                                            <a href="/course/view.php?id=11" class="col-md-3 col-sm-6 col-xs-12 text-center ">
                                                <span class="fa-stack fa-5x">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-bar-chart fa-stack-1x fa-inverse"></i>
                                                </span>
                                                <h3 class="text-center">Mate 1 CABA y 2 prov.</h3>
                                            </a>
                                            <a href="/course/view.php?id=25" class="col-md-3 col-sm-6 col-xs-12 text-center ">
                                                <span class="fa-stack fa-5x">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-percent fa-stack-1x fa-inverse"></i>
                                                </span>
                                                <h3 class="text-center">Mate 2 CABA y 3 prov</h3>
                                            </a>
                                            <a href="/course/view.php?id=24" class="col-md-3 col-sm-6 col-xs-12 text-center ">
                                                <span class="fa-stack fa-5x">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-minus-square fa-stack-1x fa-inverse"></i>
                                                </span>
                                                <h3 class="text-center">Mate 3 CABA y 4 prov. </h3>
                                            </a>
                                            <a href="/course/view.php?id=24" class="col-md-3 col-sm-6 col-xs-12 text-center ">
                                                <span class="fa-stack fa-5x">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-plus-square fa-stack-1x fa-inverse"></i>
                                                </span>
                                                <h3 class="text-center">Mate 4 CABA y 5 prov.</h3>
                                            </a>
                                            <a href="/course/view.php?id=28" class="col-md-3 col-sm-6 col-xs-12 text-center ">
                                                <span class="fa-stack fa-5x">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-diamond fa-stack-1x fa-inverse"></i>
                                                </span>
                                                <h3 class="text-center">Mate 5 CABA y 6 prov.</h3>
                                            </a>
                                            <a href="/course/view.php?id=28" class="col-md-3 col-sm-6 col-xs-12 text-center ">
                                                <span class="fa-stack fa-5x">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-audio-description fa-stack-1x fa-inverse"></i>
                                                </span>
                                                <h3 class="text-center">Aula Docente.</h3>
                                            </a>
                                        </div>
                                    </div>
                        </section>