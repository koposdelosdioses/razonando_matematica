<section class="success" id="Próximas Novedades">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2>Proximas novedades</h2>
        <hr class="star-light">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-lg-offset-2">
        <p class="text-justify">
          Aquí podes descargar novedades, actualizaciones, y demas cosas que encontremos de tu interes.
        </p>
      </div>
      <div class="col-lg-4">
        <p class="text-justify">
          <ul>
            <li>
              <b>Actividades de ciencias</b>: Matemática - Programación </li>
            <li>
              <b>Competencias y Torneos</b>: Fechas - Autorizaciones .</li>
          </ul>.
        </p>
      </div>
      <div class="col-lg-8 col-lg-offset-2 text-center">
        <a target="_blank" href="pdf/" class="btn btn-lg btn-outline">
          <i class="fa fa-download"></i> Algo nuevo 1
        </a>
        <a target="_blank" href="pdf/" class="btn btn-lg btn-outline">
          <i class="fa fa-download"></i> Algo nuevo 2
        </a>
        <a target="_blank" href="pdf/" class="btn btn-lg btn-outline">
          <i class="fa fa-download"></i> Algo nuevo 3
        </a>
      </div>
    </div>
  </div>
  </div>
  </div>
</section>