<nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
  <div class="container">
    <div class="navbar-header">
      <!-- /.boton mobile -->
      <a class="navbar-brand" href="#">
        <img class="logo-img" src="img/abaciclub.png" width="60" alt="logotipo">
        <p class="logo">Abaci club</p>
      </a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <!-- Listado de items -->
      <ul class="nav navbar-nav navbar-right">
        <li class="page-scroll">

          <a href="#paginaprincipal">
            <i class="material-icons">home</i>
          </a>
        </li>
        <li class="page-scroll">
          <a href="#Proximas novedades">Proximas novedades</a>
        </li>
        <li class="page-scroll">
          <a href="#contactanos">Contactanos</a>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
</nav>