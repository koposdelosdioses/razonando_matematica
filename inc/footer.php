<section id="contactanos">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h2>Contactanos</h2>
          <hr class="star-primary">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
          <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
          <form name="sentMessage" id="contactForm" novalidate>
            <div class="row control-group">
              <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Name</label>
                <input type="text" class="form-control" placeholder="Nombre" id="name" required data-validation-required-message="Por favor, ingrese su name.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="row control-group">
              <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Email</label>
                <input type="email" class="form-control" placeholder="Email" id="email" required data-validation-required-message="Por favor, ingrese su email address.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="row control-group">
              <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Número de teléfono</label>
                <input type="tel" class="form-control" placeholder="Número de teléfono" id="phone" required data-validation-required-message="Por favor, ingrese su número de teléfono.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="row control-group">
              <div class="form-group col-xs-12 floating-label-form-group controls">
                <label>Mensaje</label>
                <textarea rows="5" class="form-control" placeholder="Mensaje" id="message" required data-validation-required-message="Por favor, ingrese un mensaje."></textarea>
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <br>
            <div id="success"></div>
            <div class="row">
              <div class="form-group col-xs-12">
                <button type="submit" class="btn btn-success btn-lg">Enviar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  
  <footer class="text-center footer-style">
  <div class="container">
    <div class="row">
      <div class="col-md-4 footer-col">
        <h3>Dirección</h3>
        <p>
          Ramos mejía, Bs. As.- Argentina <br /> 
        </p>
      </div>
      <div class="col-md-4 footer-col">
        <h3>Mis redes</h3>
        <ul class="list-inline">
          <li>
            <a target="_blank" href="https://www.facebook.com/Abaciclub/" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
          </li>
          <li>
            <a target="_blank" href="https://classroom.google.com/u/2/h" class="btn-social btn-outline"><i><i class="fa fa-university" aria-hidden="true"></i></i></a>
          </li>
        </ul>
      </div>
      <div class="col-md-4 footer-col">
        <h3>Docencia en Matemática</h3>
        <p>Comprender, disfrutar y transmitir.</p>
      </div>
    </div>
  </div>
</footer>