 $(document).ready(function () {
     $(".owl-carousel").owlCarousel({
         items: 1,
         loop: true,
         autoplay: true,
         autoplayTimeout: 6000,
     });
 });


 function primo() {
     var valor = parseInt($("#primo").val());
     var NumeroPrimo = true;
     if (isNaN(valor)) {
         $("#resultado").text("algo no anda bien");
         return;
     }
     for (i = 1; i < valor; i++) {
         // If factor
         if (valor / i == Math.round(valor / i) && i != 1 && i != valor) {
             NumeroPrimo = false;
             break;
         };
     };

     if (NumeroPrimo)
         $("#resultado").text("El numero " + valor + " es Primo");
     else
         $("#resultado").text("El numero " + valor + " NO es Primo");
 }

 $("#boton").click(primo);


 